### 说明
-->由于大文件在Git上必须使用Large File Storage(LFS)管理器，否则会拒收，故均采用5MB每包分卷压缩，下载后使用合适的压缩软件随便解压其中一个分卷就好。
### 目录

- gcc-arm
  >Arm平台与win平台的交叉编译工具，使用Makefile方式编译时需要用到，同时也是一些IDE的必要依赖  
  安装完成后**务必勾选`add to path`复选框**，否则需要手动添加环境变量

- Keil Ue5
  >里面包含了Keil Ue5和它的破解文件，版本号为MDK533(2021/07/15)  
  装好本体后打破解补丁即可  
  [关于破解和安装](https://jingyan.baidu.com/article/fdffd1f81e500eb2e98ca193.html)  

- openOCD
  >OpenOCD（Open On-Chip Debugger）是一个开源的片上调试器，旨在提供针对嵌入式设备的调试、系统编程和边界扫描功能，能够替代`Keil`等IDE的内置下载功能。  

- STM32-CubeMX
  >ST官方推出的STM32图形化工程生成软件，可以图形化生成HAL库和LL库的代码，支持`Keil MDK`与`Makefile`等诸多开发方式  

- Git
  >版本控制系统(都到这里找资源了真的不知道这是啥嘛?)    
